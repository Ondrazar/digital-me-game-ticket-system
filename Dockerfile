FROM node:8.9.4-alpine
LABEL maintainer="Mehrad Rafigh <mehrad.rafigh@viadee.de>"
RUN npm install -g pushstate-server
RUN mkdir -p /app/dist
WORKDIR /app
COPY ./dist ./dist
CMD pushstate-server dist/ 4205
EXPOSE 4205
