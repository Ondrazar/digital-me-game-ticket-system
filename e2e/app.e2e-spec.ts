import { DigitalMeGameTicketSystemPage } from './app.po';

describe('digital-me-game-ticket-system App', () => {
  let page: DigitalMeGameTicketSystemPage;

  beforeEach(() => {
    page = new DigitalMeGameTicketSystemPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('dmticket works!');
  });
});
