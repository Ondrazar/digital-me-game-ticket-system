import { browser, element, by } from 'protractor';

export class DigitalMeGameTicketSystemPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('dmticket-root h1')).getText();
  }
}
