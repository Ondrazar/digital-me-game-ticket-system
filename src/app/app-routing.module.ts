import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './components/general/landing-page/landing-page.component';
import { MainMenuComponent } from './components/general/main-menu/main-menu.component';
import { IntroductionComponent } from './components/general/introduction/introduction.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent
  },
  {
    path: 'menu',
    component: MainMenuComponent
  },
  {
    path: 'introduction',
    component: IntroductionComponent
  },
  {
    path: 'level',
    loadChildren: './components/game/game.module#GameModule'
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
