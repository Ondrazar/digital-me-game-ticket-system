import { Component } from '@angular/core';

@Component({
  selector: 'dmticket-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent {
}
