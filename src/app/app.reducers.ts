import { ActionReducerMap } from '@ngrx/store';

import * as gameReducers from './components/game/store/game.reducers';
import * as gameActions from './components/game/store/game.actions';

export interface AppState {
  game: gameReducers.State,
}

export type AppActions = gameActions.GameActions;

export const reducers: ActionReducerMap<AppState, AppActions> = {
  game: gameReducers.gameReducer,
};
