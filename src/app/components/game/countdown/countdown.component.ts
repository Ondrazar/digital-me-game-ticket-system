import { Component, Input } from '@angular/core';

@Component({
  selector: 'dmticket-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent {
  @Input() countdown = 4;
  @Input() visible = false;
}
