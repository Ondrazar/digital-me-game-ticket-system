import { Component, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'dmticket-footer-game',
  templateUrl: './footer-game.component.html',
  styleUrls: ['./footer-game.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterGameComponent {

  @Input() paused = false;

  @Output() pauseGame = new EventEmitter();
  @Output() resumeGame = new EventEmitter();
}
