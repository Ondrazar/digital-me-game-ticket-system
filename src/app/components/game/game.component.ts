import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { interval } from 'rxjs/observable/interval';
import { timer } from 'rxjs/observable/timer';
import { take, map } from 'rxjs/operators';

import { Ticket } from '../../models/ticket.model';
import { Result } from '../../models/result.model';
import { GameOverReason } from '../../enums/game-over-reason.enum';
import { Profession } from '../../enums/profession.enum';
import { Priority } from '../../enums/priority.enum';

import * as GameActions from './store/game.actions';
import * as fromApp from '../../app.reducers';

@Component({
  selector: 'dmticket-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {

  // All levels
  private readonly maxTickets: number = 5;
  private readonly maxMistakes: number = 3;
  private readonly startingTickets: number = 1;
  private readonly gameDuration: number = 60;
  private readonly countdownDuration: number = 4;

  private ticketInterval!: number;
  private incorrectTicketInterval!: number;

  private gameTimer$: Subscription = new Subscription();
  private countdownTimer$: Subscription = new Subscription();
  private gameState$: Subscription = new Subscription();
  private ticketInterval$: Subscription = new Subscription();
  private incorrectTicketInterval$: Subscription = new Subscription();

  public showTutorial = true;
  public level!: number;
  public gameTimer = this.gameDuration;
  public countdownTimer = this.countdownDuration;
  public tickets: Ticket[] = [];
  public wrongTicketsInQueue = false;
  public gameInProgress = false;
  public gamePaused = false;
  public result: Result = new Result(0, 0, 0);
  public gameOverReason: GameOverReason = GameOverReason.Victory;

  // Level 1
  private readonly gameDurationLevelOne: number = 30;

  // Level 3
  public readonly numberOfAdministrators: number = 2;
  public readonly numberOfProgrammers: number = 2;
  public readonly startingCapacity: number = 2; // == Workload.Low

  public decreaseWorkloadInterval!: number;

  public activeTicket: Ticket | undefined = undefined;
  public activeTicketPriority: Priority | undefined = undefined;

  constructor(private store: Store<fromApp.AppState>, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.levelSettings();
  }

  private levelSettings() {
    this.route.params.subscribe(params => {
      this.level = +params['lvl'];
    });

    switch (this.level) {
      case 1: {
        this.ticketInterval = 6010; // So the game doesn't end on QueueOverflow if nothing is assigned
        this.incorrectTicketInterval = 1500;
        break;
      }
      case 2: {
        this.ticketInterval = 6500;
        this.incorrectTicketInterval = 5500;
        break;
      }
      case 3: {
        this.ticketInterval = 7000;
        this.incorrectTicketInterval = 6000;
        this.decreaseWorkloadInterval = 24000;
        break;
      }
      default: this.router.navigate(['menu']);
    }
  }

  private resetTimer() {
    if (this.level === 1) {
      this.gameTimer = this.gameDurationLevelOne;
    } else {
      this.gameTimer = this.gameDuration;
    }

    this.countdownTimer = this.countdownDuration;
  }

  private initialCountdown() {
    this.countdownTimer$ = timer(1000, 1000).pipe(
      take(this.countdownTimer),
      map(() => --this.countdownTimer)).subscribe(
      timeLeft => {
        if (timeLeft === 0) {
          this.initialTicketAction();
          this.initializeTimerSubscription();
          this.initializeTicketIntervalSubscription();
        }
      }
    );
  }

  private newGameAction() {
    if (this.level === 3) {
      this.store.dispatch(new GameActions.NewGame(
        {administratorCapacity: this.numberOfAdministrators * this.startingCapacity,
         programmerCapacity: this.numberOfProgrammers * this.startingCapacity}
      ));
    } else {
      this.store.dispatch(new GameActions.NewGame());
    }
  }

  private initialTicketAction() {
    for (let i = 0; i < this.startingTickets; i++) {
      this.store.dispatch(new GameActions.GetTicket());
    }
  }

  private initializeStateSubscription() {
    this.gameState$ = this.store.select('game').subscribe(
      (state) => {
        this.tickets = state.inGameTickets;
        this.wrongTicketsInQueue = state.incorrectlyAssignedTickets.length !== 0
        this.gameInProgress = state.inProgress;
        this.gamePaused = state.paused;
        this.result = new Result(state.correctlyAssigned, state.incorrectlyAsssigned, state.correctPriorities);

        this.activeTicket = state.activeTicket;
      }
    );
  }

  private initializeTimerSubscription() {
    this.gameTimer$ = timer(1000, 1000).pipe(
      take(this.gameTimer),
      map(() => --this.gameTimer)).subscribe(
      timeLeft => {
        if (timeLeft === 0) {
          if (this.level === 1 && this.tickets.length !== 0) {
            this.gameOver(GameOverReason.NotAllTicketsAssigned);
          } else {
            this.gameOver(GameOverReason.Victory);
          }
        }
      }
    );
  }

  private initializeTicketIntervalSubscription() {
    this.ticketInterval$ = interval(this.ticketInterval).subscribe(
      () => {
        if (this.tickets.length === this.maxTickets) {
          this.gameOver(GameOverReason.QueueOverflow);
        } else {
          this.store.dispatch(new GameActions.GetTicket())
        }
      }
    );

    this.incorrectTicketInterval$ = interval(this.incorrectTicketInterval).subscribe(
      () => {
        if (this.wrongTicketsInQueue) {
          if (this.tickets.length === this.maxTickets) {
            this.gameOver(GameOverReason.QueueOverflow);
          } else {
            this.store.dispatch(new GameActions.GetIncorrectlyAssignedTicket())
          }
        }
      }
    );
  }

  private gameOver(gameOverReason: GameOverReason) {
    this.gameOverReason = gameOverReason;
    this.store.dispatch(new GameActions.GameOver());
    this.unsubscribeAll();
  }

  private unsubscribeAll() {
    this.countdownTimer$.unsubscribe();
    this.gameTimer$.unsubscribe();
    this.gameState$.unsubscribe();
    this.ticketInterval$.unsubscribe();
    this.incorrectTicketInterval$.unsubscribe();
  }

  private assignPriority(ticket: Ticket, priority: Priority) {
    if (this.gameInProgress && !this.gamePaused) {
      this.store.dispatch(new GameActions.AssignPriority({ticket, priority}));
    }
  }

  assignTicket(ticket: Ticket | undefined, isCorrect: boolean) {
    if (this.gameInProgress && !this.gamePaused && ticket) {
      this.store.dispatch(new GameActions.AssignTicket({ticket, isCorrect}));

      if ((this.level === 2 || this.level === 3) && isCorrect && this.activeTicketPriority) {
        this.assignPriority(ticket, this.activeTicketPriority);
      }
    }

    if (this.result.incorrectlyAssigned === this.maxMistakes) {
      this.gameOver(GameOverReason.TooManyMistakes);
    }

    if (this.level === 1 && this.result.correctlyAssigned === 5) {
      this.gameOver(GameOverReason.Victory);
    }
  }

  selectActiveTicket(ticket: Ticket, priority: Priority) {
    this.activeTicketPriority = priority;

    if (this.gameInProgress && !this.gamePaused) {
      this.store.dispatch(new GameActions.SetActiveTicket(ticket));
    }
  }

  freeCapacity(profession: Profession) {
    if (this.gameInProgress && !this.gamePaused) {
      this.store.dispatch(new GameActions.FreeCapacity(profession));
    }
  }

  closeTutorial() {
    this.showTutorial = false;
    this.newGame();
  }

  newGame() {
    this.resetTimer();
    this.newGameAction();
    this.initializeStateSubscription();
    this.initialCountdown();
  }

  pauseGame() {
    this.store.dispatch(new GameActions.PauseGame());
    this.gameTimer$.unsubscribe();
    this.ticketInterval$.unsubscribe();
    this.incorrectTicketInterval$.unsubscribe();
  }

  resumeGame() {
    this.store.dispatch(new GameActions.ResumeGame());
    this.initializeTimerSubscription();
    this.initializeTicketIntervalSubscription();
  }

  ngOnDestroy() {
    this.unsubscribeAll();
  }

}
