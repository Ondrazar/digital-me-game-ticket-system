import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game.component';
import { TicketComponent } from './ticket/ticket.component';
import { WorkersTableComponent } from './workers-table/workers-table.component';
import { WorkerRowComponent } from './workers-table/worker-row/worker-row.component';
import { HeaderGameComponent } from './header-game/header-game.component';
import { FooterGameComponent } from './footer-game/footer-game.component';
import { GameOverModalComponent } from './modals/game-over-modal/game-over-modal.component';
import { GameMenuModalComponent } from './modals/game-menu-modal/game-menu-modal.component';
import { TutorialModalComponent } from './modals/tutorial-modal/tutorial-modal.component';
import { PriorityDropdownComponent } from './priority-dropdown/priority-dropdown.component';
import { CountdownComponent } from './countdown/countdown.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { TutorialContentComponent } from './tutorial/tutorial-content/tutorial-content.component';

const routes: Routes = [
  { path: ':lvl', component: GameComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    GameComponent,
    TicketComponent,
    WorkersTableComponent,
    WorkerRowComponent,
    HeaderGameComponent,
    FooterGameComponent,
    GameOverModalComponent,
    GameMenuModalComponent,
    TutorialModalComponent,
    PriorityDropdownComponent,
    CountdownComponent,
    TutorialComponent,
    TutorialContentComponent
]
})
export class GameModule { }
