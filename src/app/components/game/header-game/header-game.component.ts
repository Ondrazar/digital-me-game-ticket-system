import { Component, Input, trigger, state, style, OnChanges, transition, animate } from '@angular/core';
import { Result } from '../../../models/result.model';

@Component({
  selector: 'dmticket-header-game',
  templateUrl: './header-game.component.html',
  styleUrls: ['./header-game.component.scss'],
  animations: [
    trigger('tickState', [
      state('normal', style({
        transform: 'scale(1)'
      })),
      state('scaled', style({
        transform: 'scale(1.2)'
      })),
      transition('normal <=> scaled', animate('150ms ease-in'))
    ]),
    trigger('crossState', [
      state('normal', style({
        transform: 'translateX(0)'
      })),
      state('right', style({
        transform: 'translateX(3px)'
      })),
      state('left', style({
        transform: 'translateX(-3px)'
      })),
      transition('normal => right', animate('100ms ease-in')),
      transition('right => left', animate('100ms ease-in')),
      transition('left => normal', animate('100ms ease-in')),
    ])
  ]
})
export class HeaderGameComponent implements OnChanges {

  @Input() result!: Result;
  @Input() countdown = 60;
  @Input() gameInProgress = true;

  public tickState = 'normal';
  public crossState = 'normal';

  private correctlyAssigned = 0;
  private incorrectlyAssigned = 0;

  ngOnChanges() {
    if (this.result.correctlyAssigned === 0 && this.result.incorrectlyAssigned === 0) {
      this.correctlyAssigned = 0;
      this.incorrectlyAssigned = 0;
    }

    if (this.result.correctlyAssigned > this.correctlyAssigned) {
      this.tickAnimation();
      this.correctlyAssigned = this.result.correctlyAssigned;
    }

    if (this.result.incorrectlyAssigned > this.incorrectlyAssigned) {
      this.crossAnimation();
      this.incorrectlyAssigned = this.result.incorrectlyAssigned;
    }
  }

  private tickAnimation() {
    this.tickState = 'scaled';
    setTimeout(
      () => {
        this.tickState = 'normal';
      }, 150
    );
  }

  private crossAnimation() {
    this.crossState = 'right';
    setTimeout(
      () => {
        this.crossState = 'left';
        setTimeout(
          () => {
            this.crossState = 'normal';
          }, 100
        );
      }, 100
    );
  }

}
