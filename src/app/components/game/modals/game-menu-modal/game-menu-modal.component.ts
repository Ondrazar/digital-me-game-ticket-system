import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'dmticket-game-menu-modal',
  templateUrl: './game-menu-modal.component.html',
  styleUrls: ['./game-menu-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameMenuModalComponent implements OnInit {
  @Input() visible = false;

  @Output() resumeGame = new EventEmitter();

  public level = 1;
  public showTutorial = false;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.level = +params['lvl'];
    });
  }

  onShowTutorial() {
    this.showTutorial = !this.showTutorial;
  }
}
