import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { GameOverReason } from '../../../../enums/game-over-reason.enum';
import { Result } from '../../../../models/result.model';

@Component({
  selector: 'dmticket-game-over-modal',
  templateUrl: './game-over-modal.component.html',
  styleUrls: ['./game-over-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameOverModalComponent {

  @Input() level = 1;
  @Input() gameOverReason!: GameOverReason;
  @Input() visible = false;
  @Input() result!: Result;

  @Output() newGame = new EventEmitter();
}
