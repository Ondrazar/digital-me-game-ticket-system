import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'dmticket-tutorial-modal',
  templateUrl: './tutorial-modal.component.html',
  styleUrls: ['./tutorial-modal.component.scss']
})
export class TutorialModalComponent {

  @Input() level = 1;
  @Input() visible = false;

  @Output() close = new EventEmitter();
}
