import { Component, ChangeDetectionStrategy, Output, EventEmitter, ViewChild, ElementRef, Input, OnChanges } from '@angular/core';
import { Priority } from '../../../enums/priority.enum';

@Component({
  selector: 'dmticket-priority-dropdown',
  templateUrl: './priority-dropdown.component.html',
  styleUrls: ['./priority-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PriorityDropdownComponent implements OnChanges {
  @ViewChild('selectedPriority') selectedPriority!: ElementRef;

  @Input() active = false;

  @Output() assignPriority = new EventEmitter<Priority>();

  public selectPriorityLabel = 'Priorität zuweisen';
  public visible = false;
  public priority = Priority;
  public prioritySelected = false;

  ngOnChanges() {
    if (!this.active) {
      this.visible = false;
    }
  }

  onDropdown() {
    this.visible = !this.visible;
  }

  onAssignPriority(priority: Priority) {
    this.assignPriority.emit(priority);
    this.visible = false;
    this.prioritySelected = true;
    this.changeSelectedLabel(priority);
  }

  private changeSelectedLabel(priority: Priority) {
    switch (priority) {
      case 'Low': {
        this.selectPriorityLabel = 'Niedrig';
        this.selectedPriority.nativeElement.style.backgroundColor = '#b0de35'; // lighten($success, 10%);
        break;
      }
      case 'Medium': {
        this.selectPriorityLabel = 'Mittel';
        this.selectedPriority.nativeElement.style.backgroundColor = '#ffd833'; // lighten($warning, 10%);
        break;
      }
      case 'High': {
        this.selectPriorityLabel = 'Hoch';
        this.selectedPriority.nativeElement.style.backgroundColor = '#eb7152'; // lighten($danger, 10%);
        break;
      }
      default: {}
    }
  }

}
