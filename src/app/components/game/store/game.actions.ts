import { Ticket } from '../../../models/ticket.model';
import { Action } from '@ngrx/store';
import { Profession } from '../../../enums/profession.enum';
import { Priority } from '../../../enums/priority.enum';

export const ASSIGN_TICKET = 'ASSIGN_TICKET';
export const ASSIGN_PRIORITY = 'ASSIGN_PRIORITY';
export const GET_TICKET = 'GET_TICKET';
export const GET_INCORRECTLY_ASSIGNED_TICKET = 'GET_WRONGLY_ASSIGNED_TICKET';
export const SET_ACTIVE_TICKET = 'SET_ACTIVE_TICKET';
export const FREE_CAPACITY = 'FREE_CAPACITY';
export const NEW_GAME = 'NEW_GAME';
export const GAME_OVER = 'GAME_OVER';
export const PAUSE_GAME = 'PAUSE_GAME';
export const RESUME_GAME = 'RESUME_GAME';

export class AssignTicket implements Action {
    readonly type = ASSIGN_TICKET;

    constructor(public payload:  {ticket: Ticket, isCorrect: boolean}) {}
}

export class AssignPriority implements Action {
    readonly type = ASSIGN_PRIORITY;

    constructor(public payload: {ticket: Ticket, priority: Priority}) {}
}

export class GetTicket implements Action {
    readonly type = GET_TICKET;

    constructor() {}
}

export class GetIncorrectlyAssignedTicket implements Action {
    readonly type = GET_INCORRECTLY_ASSIGNED_TICKET;

    constructor() {}
}

export class SetActiveTicket implements Action {
    readonly type = SET_ACTIVE_TICKET;

    constructor(public payload: Ticket) {}
}

export class FreeCapacity implements Action {
    readonly type = FREE_CAPACITY;

    constructor(public payload: Profession) {}
}

export class NewGame implements Action {
    readonly type = NEW_GAME;

    constructor(public payload?: {administratorCapacity: number, programmerCapacity: number}) {}
}

export class GameOver implements Action {
    readonly type = GAME_OVER;

    constructor() {}
}

export class PauseGame implements Action {
    readonly type = PAUSE_GAME;

    constructor() {}
}

export class ResumeGame implements Action {
    readonly type = RESUME_GAME;

    constructor() {}
}

export type GameActions = AssignTicket | AssignPriority | GetTicket | GetIncorrectlyAssignedTicket | SetActiveTicket |
FreeCapacity | NewGame | GameOver | PauseGame | ResumeGame;
