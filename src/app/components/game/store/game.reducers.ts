import { Ticket } from '../../../models/ticket.model';
import { Profession } from '../../../enums/profession.enum';

import * as GameActions from './game.actions';

const ticketJSON: Ticket[] = require('../../../../assets/json/tickets.json');
const surnames: string[] = require('../../../../assets/json/surnames.json').surnames;

export interface State {
    inGameTickets: Ticket[];
    incorrectlyAssignedTickets: Ticket[];
    ticketQueue: Ticket[];
    correctlyAssigned: number;
    incorrectlyAsssigned: number;
    correctPriorities: number;
    inProgress: boolean;
    paused: boolean;
    activeTicket: Ticket | undefined;
    administratorCapacity: number;
    programmerCapacity: number;
}

const initialState: State =  {
    inGameTickets: [],
    incorrectlyAssignedTickets: [],
    ticketQueue: [],
    correctlyAssigned: 0,
    incorrectlyAsssigned: 0,
    correctPriorities: 0,
    inProgress: false,
    paused: false,
    activeTicket: undefined,
    administratorCapacity: 999,
    programmerCapacity: 999
}

export function gameReducer(state: State = initialState, action: GameActions.GameActions) {
    switch (action.type) {
        case (GameActions.ASSIGN_TICKET):
            return {
                ...state,
                inGameTickets: state.inGameTickets.filter(t => t.id !== action.payload.ticket.id),
                correctlyAssigned: action.payload.isCorrect ? ++state.correctlyAssigned : state.correctlyAssigned,
                incorrectlyAssignedTickets: action.payload.isCorrect ?
                                        state.incorrectlyAssignedTickets : [...state.incorrectlyAssignedTickets, action.payload.ticket],
                incorrectlyAsssigned: action.payload.isCorrect ? state.incorrectlyAsssigned :  ++state.incorrectlyAsssigned,
                activeTicket: undefined,
                administratorCapacity: action.payload.isCorrect && (action.payload.ticket.profession === Profession.Administrator) ?
                                       --state.administratorCapacity : state.administratorCapacity,
                programmerCapacity: action.payload.isCorrect && (action.payload.ticket.profession === Profession.Programmer) ?
                                    --state.programmerCapacity : state.programmerCapacity,
            };
        case (GameActions.ASSIGN_PRIORITY):
            if (action.payload.ticket.priority === action.payload.priority) {
              return {
                ...state,
                correctPriorities: ++state.correctPriorities
              }
            } else {
              return {
                ...state
              }
            }
        case (GameActions.GET_TICKET):
            if (state.ticketQueue.length !== 0) {
                let ticket: Ticket | undefined = undefined;
                if (state.administratorCapacity === 0) {
                    ticket = state.ticketQueue.find(t => t.profession === Profession.Programmer);
                } else if (state.programmerCapacity === 0) {
                    ticket = state.ticketQueue.find(t => t.profession === Profession.Administrator);
                }
                if (ticket === undefined) {
                    ticket = state.ticketQueue[0];
                }
                return {
                ...state,
                inGameTickets: [...state.inGameTickets, ticket],
                ticketQueue: state.ticketQueue.filter(t => t.id !== ticket!.id)
                };
            } else {
                return state;
            }
        case (GameActions.GET_INCORRECTLY_ASSIGNED_TICKET):
            if (state.incorrectlyAssignedTickets.length !== 0) {
                const ticket = state.incorrectlyAssignedTickets[0];
                return {
                ...state,
                inGameTickets: [...state.inGameTickets, ticket],
                incorrectlyAssignedTickets: state.incorrectlyAssignedTickets.filter(t => t.id !== ticket.id)
                };
            } else {
                return state;
            }
        case (GameActions.SET_ACTIVE_TICKET):
            return {
                ...state,
                activeTicket: action.payload,
            };
        case (GameActions.FREE_CAPACITY):
            return {
                ...state,
                administratorCapacity: action.payload === Profession.Administrator ?
                                       ++state.administratorCapacity : state.administratorCapacity,
                programmerCapacity: action.payload === Profession.Programmer ?
                                    ++state.programmerCapacity : state.programmerCapacity,
            }
        case (GameActions.NEW_GAME):
            return {
                ...state,
                inGameTickets: [],
                incorrectlyAssignedTickets: [],
                ticketQueue: shuffleTickets(generateSubmitter(ticketJSON)),
                correctlyAssigned: 0,
                incorrectlyAsssigned: 0,
                correctPriorities: 0,
                inProgress: true,
                paused: false,
                activeTicket: undefined,
                administratorCapacity: action.payload ? action.payload.administratorCapacity : 999, // arbitrary number
                programmerCapacity: action.payload ? action.payload.programmerCapacity : 999 // arbitrary number
            };
        case (GameActions.GAME_OVER):
            return {
                ...state,
                ticketQueue: [],
                inProgress: false
            };
        case (GameActions.PAUSE_GAME):
            return {
                ...state,
                paused: true
            }
        case (GameActions.RESUME_GAME):
            return {
                ...state,
                paused: false
            }
        default:
            return state;
    }
}

function shuffleTickets(tickets: Ticket[]): Ticket[] {
    for (let i = tickets.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [tickets[i], tickets[j]] = [tickets[j], tickets[i]];
    }
    return tickets;
}

function generateSubmitter(tickets: Ticket[]): Ticket[] {
    const ticketsWithSurnames: Ticket[] = JSON.parse(JSON.stringify(tickets)); // deep copy

    ticketsWithSurnames.forEach(
        ticket => {
            const gender = Math.floor(Math.random() * Math.floor(2)) % 2 === 1 ? 'Hr. ' : 'Fr. ';
            const surname = surnames[Math.floor(Math.random() * surnames.length)];
            ticket.submittedBy = gender.concat(surname).concat(' ').concat(ticket.submittedBy).trim();
        }
    );

    return ticketsWithSurnames;
}
