import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Ticket } from '../../../models/ticket.model';
import { Profession } from '../../../enums/profession.enum';
import { Priority } from '../../../enums/priority.enum';

@Component({
  selector: 'dmticket-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TicketComponent {

  @Input() level!: number;
  @Input() ticket!: Ticket;
  @Input() active = false;

  @Output() assignTicket = new EventEmitter<boolean>();
  @Output() selectActiveTicket = new EventEmitter<Priority>();

  public profession = Profession;
  public professionAssigned = false;

  public selectedPriority: Priority | undefined = undefined;

  onAssignProfession(profession: Profession) {
    if (profession === this.ticket.profession) {
      switch (this.level) {
        case 1: {
          this.assignTicket.emit(true);
          break;
        }
        case 2: {
          this.professionAssigned = true;
          break;
        }
        default: {}
      }
    } else {
      this.assignTicket.emit(false);
    }
  }

  onAssignPriority(priority: Priority) {
    this.selectedPriority = priority;
    this.selectActiveTicket.emit(priority);

    if (this.level === 2) {
      this.assignTicket.emit(true);
    }
  }

  isLevelThree(): boolean {
    return this.level === 3;
  }

}
