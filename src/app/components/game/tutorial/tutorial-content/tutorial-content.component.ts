import { Component, Input } from '@angular/core';

@Component({
  selector: 'dmticket-tutorial-content',
  templateUrl: './tutorial-content.component.html',
  styleUrls: ['./tutorial-content.component.scss']
})
export class TutorialContentComponent {

  @Input() level!: number;
}
