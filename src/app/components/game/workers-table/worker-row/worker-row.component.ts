import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Workload } from '../../../../enums/workload.enum';
import { Profession } from '../../../../enums/profession.enum';
import { Worker } from '../../../../models/worker.model';
import { Ticket } from '../../../../models/ticket.model';

@Component({
  selector: 'dmticket-worker-row',
  templateUrl: './worker-row.component.html',
  styleUrls: ['./worker-row.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkerRowComponent {

  @Input() active = false;
  @Input() gameInProgress = false;
  @Input() decreaseWorkloadInterval = 10000;
  @Input() worker!: Worker;
  @Input() ticket: Ticket | undefined = undefined;

  @Output() assignTicket = new EventEmitter<boolean>();
  @Output() freeCapacity = new EventEmitter<Profession>();

  public workload = Workload;
  public profession = Profession;

  onAssignTicket() {
    if (this.active) {
      if (this.worker.workload !== this.workload.High) {
        if (this.ticket && this.worker.profession === this.ticket.profession) {
          this.worker.increaseWorkload();
          this.setUpDecreaseWorkload();
          this.assignTicket.emit(true);
        } else {
          this.assignTicket.emit(false);
        }
      }
    }
  }

  private setUpDecreaseWorkload() {
    setTimeout(
      () => {
        if (this.gameInProgress) {
          this.worker.decreaseWorkload();
          this.freeCapacity.emit(this.worker.profession);
        }
      }, this.decreaseWorkloadInterval
    );
  }

}
