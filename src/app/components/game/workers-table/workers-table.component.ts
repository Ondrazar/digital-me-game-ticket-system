import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Worker } from '../../../models/worker.model';
import { Profession } from '../../../enums/profession.enum';
import { Ticket } from '../../../models/ticket.model';

@Component({
  selector: 'dmticket-workers-table',
  templateUrl: './workers-table.component.html',
  styleUrls: ['./workers-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkersTableComponent {

  @Input() level!: number;
  @Input()
    set gameInProgress(gameInProgress: boolean) {
      if (this._gameInProgress === false && gameInProgress === true) {
      this.generateWorkers();
      }

      this._gameInProgress = gameInProgress;
    }

    get gameInProgress() {
      return this._gameInProgress;
    }
  @Input() numberOfAdministrators = 2;
  @Input() numberOfProgrammers = 2;
  @Input() startingCapacity = 2;
  @Input() decreaseWorkloadInterval = 20000;
  @Input() active = false;
  @Input() ticket: Ticket | undefined = undefined;

  @Output() assignTicket = new EventEmitter<boolean>();
  @Output() freeCapacity = new EventEmitter<Profession>();

  // see sprite.svg
  private readonly minSVGId = 0;
  private readonly maxSVGId = 13;

  private _gameInProgress = false;

  public administrators: Worker[] = [];
  public programmers: Worker[] = [];

  private generateWorkers() {
    this.administrators = [];
    this.programmers = [];
    const svgs = this.generateSVGIds();

    for (let i = 0; i < this.numberOfAdministrators; i++) {
      this.administrators.push(new Worker(i, svgs.pop()!, Profession.Administrator, this.startingCapacity))
    }

    for (let i = 0; i < this.numberOfProgrammers; i++) {
      this.programmers.push(new Worker(i + this.numberOfAdministrators, svgs.pop()!, Profession.Programmer, this.startingCapacity))
    }
  }

  private generateSVGIds(): string[] {
    const svgIds = new Set(); // ensures that there are no duplicates

    while (svgIds.size !== this.numberOfAdministrators + this.numberOfProgrammers) {
      const gender = Math.floor(Math.random() * Math.floor(2)) % 2 === 1 ? 'man' : 'woman';
      const number = Math.floor(Math.random() * (this.maxSVGId - this.minSVGId + 1)) + this.minSVGId;
      svgIds.add(gender.concat('-'.concat(number.toString())));
    }

    return Array.from(svgIds);
  }

}
