import { NgModule } from '@angular/core';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { AppRoutingModule } from '../../app-routing.module';

@NgModule({
  declarations: [
    LandingPageComponent,
    MainMenuComponent,
    IntroductionComponent
  ],
  imports: [
    AppRoutingModule
  ],
  exports: [
    LandingPageComponent,
    MainMenuComponent,
    IntroductionComponent
  ]
})
export class GeneralModule { }
