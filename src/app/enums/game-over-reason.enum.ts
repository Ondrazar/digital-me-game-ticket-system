export enum GameOverReason {
    Victory = 'Epischer Sieg!!',
    TooManyMistakes  = 'Schade, du hast zu viele Fehler gemacht.',
    QueueOverflow  = 'Schade, es waren zu viele Tickets in der Warteschlange.',
    NotAllTicketsAssigned = 'Schade, du musst alle Tickets zuweisen, um dieses Spiel zu gewinnen.'
}
