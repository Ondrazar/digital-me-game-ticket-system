export enum Profession {
    Administrator = 'Administrator',
    Programmer = 'Programmer'
}
