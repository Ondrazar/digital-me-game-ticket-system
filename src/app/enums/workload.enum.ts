export enum Workload {
    None = 'None',
    Low = 'Low',
    Medium  = 'Medium',
    High  = 'High'
}
