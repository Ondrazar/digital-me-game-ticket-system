export class Result {
    public correctlyAssigned: number;
    public incorrectlyAssigned: number;
    public correctPriorities: number;

    constructor(correctlyAssigned: number, incorrectlyAssigned: number, correctPriorities: number) {
        this.correctlyAssigned = correctlyAssigned;
        this.incorrectlyAssigned = incorrectlyAssigned;
        this.correctPriorities = correctPriorities;
    }
}
