import { Priority } from '../enums/priority.enum';
import { Profession } from '../enums/profession.enum';

export class Ticket {
    public id: number;
    public submittedBy: string;
    public priority: Priority;
    public profession: Profession;
    public description: string;

    constructor(id: number, submittedBy: string, priority: Priority, profession: Profession, description: string) {
        this.id = id;
        this.submittedBy = submittedBy;
        this.priority = priority;
        this.profession = profession;
        this.description = description;
    }
}
