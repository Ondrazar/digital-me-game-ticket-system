import { Profession } from '../enums/profession.enum';
import { Workload } from '../enums/workload.enum';

export class Worker {
    public id: number;
    public svgId: string;
    public profession: Profession;
    public workload: Workload;

    constructor(id: number, svgId: string, profession: Profession, startingCapacity: number) {
        this.id = id;
        this.svgId = svgId;
        this.profession = profession;
        this.workload = this.setInitialWorkload(startingCapacity);
    }

    private setInitialWorkload(startingCapacity: number): Workload {
      switch (startingCapacity) {
        case 0: return Workload.High;
        case 1: return Workload.Medium;
        case 2: return Workload.Low;
        case 3: return Workload.None;
        default: return Workload.Low;
      }
    }

    public increaseWorkload() {
        switch (this.workload) {
          case (Workload.None):
            this.workload = Workload.Low;
            break;
          case (Workload.Low):
            this.workload = Workload.Medium;
            break;
          case (Workload.Medium):
            this.workload = Workload.High;
            break;
          case (Workload.High):
            break;
        }
    }

    public decreaseWorkload() {
        switch (this.workload) {
          case (Workload.None):
            break;
          case (Workload.Low):
            this.workload = Workload.None;
            break;
          case (Workload.Medium):
            this.workload = Workload.Low;
            break;
        case (Workload.High):
            this.workload = Workload.Medium;
            break;
        }
    }
}
